#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define min(A, B) ((A)<(B) ? (A) : (B))
#define max(A, B) ((A)>(B) ? (A) : (B))

typedef struct
{
    int width;
    int height;
    int maxValue;
    unsigned char *data;

} image_t;

int filterType = 0;

void
parseArg(int argc, char **argv, FILE **infp, FILE **outfp)
{
    FILE *fp;

    if (argc < 3 )
    {
        goto usage;
    }

    *infp = fopen(argv[1], "rb");

    if (*infp==NULL)
    {
        fputs("Opening the input file was failend\n", stderr);
        goto usage;
    }

    *outfp = fopen(argv[2], "wb");

    if (*outfp==NULL)
    {
        fputs("Opening the output file was failend\n", stderr);
        goto usage;
    }
    if (argc > 3){
        filterType = atoi(argv[3]);
    }

    return;

usage:
    fprintf(stderr, "usage : %s <input pgm file> <output pgm file>\n", argv[0]);
    exit(1);
}


void
initImage(image_t *ptImage, int width, int height, int maxValue)
{
    ptImage->width = width;
    ptImage->height = height;
    ptImage->maxValue = maxValue;

    ptImage->data = (unsigned char *)malloc((size_t)(width * height));

    if (ptImage->data==NULL)
    {
        fputs("out of memory\n", stderr);
        exit(1);
    }
}


char *
readOneLine(char *buf, int n, FILE *fp)
{
    char *fgetsResult;

    do
    {
        fgetsResult = fgets(buf, n, fp);
    } while(fgetsResult!=NULL && buf[0]=='#');

    return fgetsResult;
}


void
readPgmRawHeader(FILE *fp, image_t *ptImage)
{
    int width, height, maxValue;
    char buf[128];

    if(readOneLine(buf, 128, fp)==NULL)
    {
        goto error;
    }
    if (buf[0]!='P' || buf[1]!='5')
    {
        goto error;
    }

    if (readOneLine(buf, 128, fp)==NULL)
    {
        goto error;
    }
    if (sscanf(buf, "%d %d", &width, &height) != 2)
    {
        goto error;
    }
    if ( width<=0 || height<=0)
    {
        goto error;
    }

    if (readOneLine(buf, 128, fp)==NULL)
    {
        goto error;
    }
    if (sscanf(buf, "%d", &maxValue) != 1)
    {
        goto error;
    }
    if ( maxValue<=0 || maxValue>=256 )
    {
        goto error;
    }

    initImage(ptImage, width, height, maxValue);

    return;

error:
    fputs("Reading PGM-RAW header was failed\n", stderr);
    exit(1);
}


void
readPgmRawBitmapData(FILE *fp, image_t *ptImage)
{
    if( fread(ptImage->data, sizeof(unsigned char),
            ptImage->width * ptImage->height, fp)
            != ptImage->width * ptImage->height )
    {
        fputs("Reading PGM-RAW bitmap data was failed\n", stderr);
        exit(1);
    }
}

void
reverseFiltering(image_t *resultImage, image_t *originalImage, int width, int height)
{
    for(int y=0; y<height; y++)
    {
        for(int x=0; x<width; x++)
        {
            resultImage->data[x+resultImage->width*y]
                = ( originalImage->maxValue
                -originalImage->data[x+originalImage->width*y] )
                *resultImage->maxValue/originalImage->maxValue;
        }
    }
}
void
prewitFiltering(image_t *resultImage, image_t *originalImage, int width, int height)
{
    double calc_data[height][width];
    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            // Image(x, y)のデータ
            unsigned char base = originalImage->data[x+originalImage->width*y];
            // 垂直方法の微分係数
            int delta_f_delta_i[3][3] = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
            delta_f_delta_i[0][0] = -1 * (y == 0 || x == 0 ? base: originalImage->data[(x-1)+originalImage->width*(y-1)]);
            delta_f_delta_i[0][1] = -1 * (y == 0 ? base: originalImage->data[x+originalImage->width*(y-1)]);
            delta_f_delta_i[0][2] = -1 * (y == 0 || x == width ? base: originalImage->data[(x+1)+originalImage->width*(y-1)]);
            delta_f_delta_i[2][0] = 1 * (y == height || x == 0 ? base: originalImage->data[(x-1)+originalImage->width*(y+1)]);
            delta_f_delta_i[2][1] = 1 * (y == height ? base: originalImage->data[x+originalImage->width*(y+1)]);
            delta_f_delta_i[2][2] = 1 * (y == height || x == width ? base: originalImage->data[(x+1)+originalImage->width*(y+1)]);
            int dfdi = 0;
            for (int i = 0; i < 3; i++){
                for (int j = 0; j < 3; j++){
                    dfdi = dfdi + delta_f_delta_i[i][j];
                }
            }
            // 水平方向の微分係数
            int delta_f_delta_j[3][3] = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
            delta_f_delta_j[0][0] = -1 * (x == 0 || y == 0 ? base : originalImage->data[(x-1)+originalImage->width*(y-1)]);
            delta_f_delta_j[1][0] = - 1 * (x == 0 ? base: originalImage->data[(x-1)+originalImage->width*y]);
            delta_f_delta_j[2][0] = -1 * (x == 0 || y == height ? base: originalImage->data[(x-1)+originalImage->width*(y+1)]);
            delta_f_delta_j[0][2] = 1 * (x == width || y == 0 ? base: originalImage->data[(x+1)+originalImage->width*(y-1)]);
            delta_f_delta_j[1][2] = 1 * (x == width ? base: originalImage->data[(x+1)+originalImage->width*y]);
            delta_f_delta_j[2][2] = 1 * (x == width || y == height ? base: originalImage->data[(x+1)+originalImage->width*(y+1)]);
            int dfdj = 0;
            for (int i = 0; i < 3; i++){
                for (int j = 0; j < 3; j++){
                    dfdj = dfdj + delta_f_delta_j[i][j];
                }
            }
            // calc_data[y][x] = abs(dfdi) + abs(dfdi);
            calc_data[y][x] = sqrt(pow(dfdi, 2) + pow(dfdj, 2));
        }
    }

    // データをずらす
    // (x_i - 最小値) / (最大値-最小値) * 255
    double max = 0;
    double min = calc_data[0][0];
    for (int i = 0; i < height; i++){
        for (int j = 0; j < width; j++){
            if (calc_data[i][j] > max){
                max = calc_data[i][j];
            }
            if (calc_data[i][j] < min){
                min = calc_data[i][j];
            }
        }
    }
    for (int y = 0; y < height; y++){
        for (int x = 0; x < width; x++){
            resultImage->data[x+originalImage->width*y] = (unsigned char)((calc_data[y][x] - min) * 255 / (max - min));
        }
    }
}

void
laplacian8Filtering(image_t *resultImage, image_t *originalImage, int width, int height)
{
    /* 8近傍ラプラシアンフィルタを実装 */
    double calc_data[height][width];
    double min_initial = 0;
    double min_minus_initial = 0;
    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            // Image(x, y)のデータ
            unsigned char base = originalImage->data[x+originalImage->width*y];
            // 垂直方法の微分係数
            int delta_f[3][3] = {};
            delta_f[0][0] = 1 * (y == 0 || x == 0 ? base: originalImage->data[(x-1)+originalImage->width*(y-1)]);
            delta_f[0][1] = 1 * (y == 0 ? base: originalImage->data[x+originalImage->width*(y-1)]);
            delta_f[0][2] = 1 * (y == 0 || x == width ? base: originalImage->data[(x+1)+originalImage->width*(y-1)]);
            delta_f[1][0] = 1 * (x == 0 ? base : originalImage->data[(x-1)+originalImage->width*y]);
            delta_f[1][1] = -8 * (originalImage->data[x+originalImage->width*y]);
            delta_f[1][2] = 1 * (x == width - 1? base: originalImage->data[(x+1)+originalImage->width*y]);
            delta_f[2][0] = 1 * (y == height || x == 0 ? base: originalImage->data[(x-1)+originalImage->width*(y+1)]);
            delta_f[2][1] = 1 * (y == height ? base: originalImage->data[x+originalImage->width*(y+1)]);
            delta_f[2][2] = 1 * (y == height || x == width ? base: originalImage->data[(x+1)+originalImage->width*(y+1)]);
            int g = 0;
            for (int i = 0; i < 3; i++){
                for (int j = 0; j < 3; j++){
                    g = g + delta_f[i][j];
                }
            }
            if (min_initial == 0 && g > 0) min_initial = g;
            if (min_minus_initial == 0 && g < 0) min_minus_initial = g;
            calc_data[y][x] = g;
        }
    }

    // データをずらす
    // (x_i - 最小値) / (最大値-最小値) * 255 かつ マイナスの値は反転
    double abs_max_minus = 0;
    double max = 0;
    double min = min_initial;
    double abs_min_minus = min_minus_initial;
    for (int i = 0; i < height; i++){
        for (int j = 0; j < width; j++){
            if (calc_data[i][j] > 0 && calc_data[i][j] > max)
                max = calc_data[i][j];
            if (calc_data[i][j] > 0 && calc_data[i][j] < min)
                min = calc_data[i][j];
            if (calc_data[i][j] < 0 && calc_data[i][j] < abs_max_minus)
                abs_max_minus = calc_data[i][j];
            if (calc_data[i][j] < 0 && calc_data[i][j] > abs_min_minus)
                abs_min_minus = calc_data[i][j];
        }
    }
    for (int y = 0; y < height; y++){
        for (int x = 0; x < width; x++){
            unsigned char result = 0;
            if (calc_data[y][x] > 0)
                result = (unsigned char)((calc_data[y][x] - min) * 255 / (max - min));
            else
                result = (unsigned char)(abs(calc_data[y][x] - abs_min_minus) * 255 / abs(abs_max_minus - abs_min_minus));
            resultImage->data[x+originalImage->width*y] = result;
        }
    }
}

void
sobelFiltering(image_t *resultImage, image_t *originalImage, int width, int height)
{
    /* Sobelフィルタ実装 */
    double calc_data[height][width];
    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            // Image(x, y)のデータ
            unsigned char base = originalImage->data[x+originalImage->width*y];
            // 垂直方法の微分係数
            int delta_f_delta_i[3][3] = {};
            delta_f_delta_i[0][0] = (-1) * ((y == 0 || x == 0) ? base: originalImage->data[(x-1)+originalImage->width*(y-1)]);
            delta_f_delta_i[0][1] = (-2) * (y == 0 ? base: originalImage->data[x+originalImage->width*(y-1)]);
            delta_f_delta_i[0][2] = (-1) * ((y == 0 || x == width-1) ? base: originalImage->data[(x+1)+originalImage->width*(y-1)]);
            delta_f_delta_i[2][0] = 1 * ((y == height-1 || x == 0 )? base: originalImage->data[(x-1)+originalImage->width*(y+1)]);
            delta_f_delta_i[2][1] = 2 * (y == height-1 ? base: originalImage->data[x+originalImage->width*(y+1)]);
            delta_f_delta_i[2][2] = 1 * ((y == height-1 || x == width )? base: originalImage->data[(x+1)+originalImage->width*(y+1)]);
            int dfdi = 0;
            for (int i = 0; i < 3; i++){
                for (int j = 0; j < 3; j++){
                    dfdi = dfdi + delta_f_delta_i[i][j];
                }
            }
            // 水平方向の微分係数
            int delta_f_delta_j[3][3] = {};
            delta_f_delta_j[0][0] = (-1) * ((x == 0 || y == 0 )? base : originalImage->data[(x-1)+originalImage->width*(y-1)]);
            delta_f_delta_j[1][0] = (-2) * (x == 0 ? base: originalImage->data[(x-1)+originalImage->width*y]);
            delta_f_delta_j[2][0] = (-1) * ((x == 0 || y == height-1) ? base: originalImage->data[(x-1)+originalImage->width*(y+1)]);
            delta_f_delta_j[0][2] = 1 * ((x == width-1 || y == 0)? base: originalImage->data[(x+1)+originalImage->width*(y-1)]);
            delta_f_delta_j[1][2] = 2 * (x == width-1 ? base: originalImage->data[(x+1)+originalImage->width*y]);
            delta_f_delta_j[2][2] = 1 * ((x == width-1 || y == height)? base: originalImage->data[(x+1)+originalImage->width*(y+1)]);
            int dfdj = 0;
            for (int i = 0; i < 3; i++){
                for (int j = 0; j < 3; j++){
                    dfdj = dfdj + delta_f_delta_j[i][j];
                }
            }
            // 結果の出力
            calc_data[y][x] = sqrt(pow(dfdi, 2) + pow(dfdj, 2));
        }
    }

    // データをずらす
    // (x_i - 最小値) / (最大値-最小値) * 255
    double max = 0;
    double min = calc_data[0][0];
    for (int i = 0; i < height; i++){
        for (int j = 0; j < width; j++){
            if (calc_data[i][j] > max){
                max = calc_data[i][j];
            }
            if (calc_data[i][j] < min){
                min = calc_data[i][j];
            }
        }
    }
    for (int y = 0; y < height; y++){
        for (int x = 0; x < width; x++){
            resultImage->data[x+originalImage->width*y] = (unsigned char)((calc_data[y][x] - min) * 255 / (max - min));
        }
    }
}

void
filteringImage(image_t *resultImage, image_t *originalImage)
{
    int     width, height;
    width = min(originalImage->width, resultImage->width);
    height = min(originalImage->height, resultImage->height);
    if (filterType == 1){
        puts("prewit filtering");
        prewitFiltering(resultImage, originalImage, width, height);
    }else if (filterType == 2) {
        puts("laplacian");
        laplacian8Filtering(resultImage, originalImage, width, height);
    }else {
        puts("sobel filtering");
        sobelFiltering(resultImage, originalImage, width, height);
    }
}

void
writePgmRawHeader(FILE *fp, image_t *ptImage)
{
    if(fputs("P5\n", fp)==EOF)
    {
        goto error;
    }

    if (fprintf(fp, "%d %d\n", ptImage->width, ptImage->height)==EOF)
    {
        goto error;
    }

    if (fprintf(fp, "%d\n", ptImage->maxValue)==EOF)
    {
        goto error;
    }

    return;

error:
    fputs("Writing PGM-RAW header was failed\n", stderr);
    exit(1);
}


void
writePgmRawBitmapData(FILE *fp, image_t *ptImage)
{
    if( fwrite(ptImage->data, sizeof(unsigned char),
            ptImage->width * ptImage->height, fp)
            != ptImage->width * ptImage->height )
    {
        fputs("Writing PGM-RAW bitmap data was failed\n", stderr);
        exit(1);
    }
}


int
main(int argc, char **argv)
{
    image_t originalImage, resultImage;
    FILE *infp, *outfp;
    parseArg(argc, argv, &infp, &outfp);

    readPgmRawHeader(infp, &originalImage);

    readPgmRawBitmapData(infp, &originalImage);

    initImage(&resultImage, originalImage.width, originalImage.height,
            originalImage.maxValue);

    filteringImage(&resultImage, &originalImage);

    writePgmRawHeader(outfp, &resultImage);

    writePgmRawBitmapData(outfp, &resultImage);

    return 0;
}
