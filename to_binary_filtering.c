#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define min(A, B) ((A)<(B) ? (A) : (B))
#define max(A, B) ((A)>(B) ? (A) : (B))

typedef struct
{
    int width;
    int height;
    int maxValue;
    unsigned char *data;

} image_t;

void
parseArg(int argc, char **argv, FILE **infp, FILE **outfp)
{
    FILE *fp;

    if (argc < 3 )
    {
        goto usage;
    }

    *infp = fopen(argv[1], "rb");


    if (*infp==NULL)
    {
        fputs("Opening the input file was failend\n", stderr);
        goto usage;
    }

    *outfp = fopen(argv[2], "wb");

    if (*outfp==NULL)
    {
        fputs("Opening the output file was failend\n", stderr);
        goto usage;
    }
    return;

usage:
    fprintf(stderr, "usage : %s <input pgm file> <output pgm file>\n", argv[0]);
    exit(1);
}


void
initImage(image_t *ptImage, int width, int height, int maxValue)
{
    ptImage->width = width;
    ptImage->height = height;
    ptImage->maxValue = maxValue;

    ptImage->data = (unsigned char *)malloc((size_t)(width * height));

    if (ptImage->data==NULL)
    {
        fputs("out of memory\n", stderr);
        exit(1);
    }
}


char *
readOneLine(char *buf, int n, FILE *fp)
{
    char *fgetsResult;

    do
    {
        fgetsResult = fgets(buf, n, fp);
    } while(fgetsResult!=NULL && buf[0]=='#');

    return fgetsResult;
}


void
readPgmRawHeader(FILE *fp, image_t *ptImage)
{
    int width, height, maxValue;
    char buf[128];

    if(readOneLine(buf, 128, fp)==NULL)
    {
        goto error;
    }
    if (buf[0]!='P' || buf[1]!='5')
    {
        goto error;
    }

    if (readOneLine(buf, 128, fp)==NULL)
    {
        goto error;
    }
    if (sscanf(buf, "%d %d", &width, &height) != 2)
    {
        goto error;
    }
    if ( width<=0 || height<=0)
    {
        goto error;
    }

    if (readOneLine(buf, 128, fp)==NULL)
    {
        goto error;
    }
    if (sscanf(buf, "%d", &maxValue) != 1)
    {
        goto error;
    }
    if ( maxValue<=0 || maxValue>=256 )
    {
        goto error;
    }

    initImage(ptImage, width, height, maxValue);

    return;

error:
    fputs("Reading PGM-RAW header was failed\n", stderr);
    exit(1);
}


void
readPgmRawBitmapData(FILE *fp, image_t *ptImage)
{
    if( fread(ptImage->data, sizeof(unsigned char),
            ptImage->width * ptImage->height, fp)
            != ptImage->width * ptImage->height )
    {
        fputs("Reading PGM-RAW bitmap data was failed\n", stderr);
        exit(1);
    }
}

int get_threshold(image_t *originalImage, int width, int height) {
    /*  クラス間分散が最大になるしきい値を大津の方法を用いて計算し返す */

    // 階調数
    int L = originalImage->maxValue + 1;
    // データ総数
    int dataTotal = width * height;
    // 階級ごとのデータ数を集める
    int hist[L];
    for (int i = 0; i < L; i++) hist[i] = 0;
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            unsigned char d = originalImage->data[x+originalImage->width*y];
            hist[d]++;
        }
    }
    double sigmaB[L];
    for (int k = 0; k < L; k++) {
        // omega0 omega1を求める
        double omega0 = 0, omega1 = 0;
        for (int i = 0; i < k + 1; i++)
            omega0 += (double)hist[i] / dataTotal;
        for (int i = k+1; i < L; i++)
            omega1 += (double)hist[i] / dataTotal;
        // mu0, mu1, muTを求めて SigmaBを算出し保存
        double mu0 = 0, mu1 = 0, muT = 0;
        for (int i = 0; i < L; i++) {
            double p_i = (double)hist[i] / dataTotal;
            mu0 += (omega0 && i < k + 1) ? (i * p_i) / omega0: 0;
            mu1 += (omega1 && i > k ) ? (i * p_i) / omega1: 0;
            muT += i * p_i;
        }
        sigmaB[k] = omega0 * pow(mu0 - muT, 2) + omega1 * pow(mu1 - muT, 2);
    }
    double sigmaBMax = 0;
    int T = 0;
    for (int i = 0; i < L; i++) {
        if (sigmaB[i] > sigmaBMax) {
            sigmaBMax = sigmaB[i];
            T = i;
        }
    }
    return T;
}

void
toBinaryFiltering(image_t *resultImage, image_t *originalImage, int width, int height)
{
    /* 2値化を実装する */
    // しきい値を算出
    int T = get_threshold(originalImage, width, height);
    // 求めたTを使って２値化
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            unsigned char d = originalImage->data[x+originalImage->width*y];
            if (d > T) {
                resultImage->data[x+originalImage->width*y] = 255;
            }else {
                resultImage->data[x+originalImage->width*y] = 0;
            }
        }
    }
}

void
filteringImage(image_t *resultImage, image_t *originalImage)
{
    int     width, height;
    width = min(originalImage->width, resultImage->width);
    height = min(originalImage->height, resultImage->height);
    toBinaryFiltering(resultImage, originalImage, width, height);
}

void
writePgmRawHeader(FILE *fp, image_t *ptImage)
{
    if(fputs("P5\n", fp)==EOF)
    {
        goto error;
    }

    if (fprintf(fp, "%d %d\n", ptImage->width, ptImage->height)==EOF)
    {
        goto error;
    }

    if (fprintf(fp, "%d\n", ptImage->maxValue)==EOF)
    {
        goto error;
    }

    return;

error:
    fputs("Writing PGM-RAW header was failed\n", stderr);
    exit(1);
}


void
writePgmRawBitmapData(FILE *fp, image_t *ptImage)
{
    if( fwrite(ptImage->data, sizeof(unsigned char),
            ptImage->width * ptImage->height, fp)
            != ptImage->width * ptImage->height )
    {
        fputs("Writing PGM-RAW bitmap data was failed\n", stderr);
        exit(1);
    }
}


int
main(int argc, char **argv)
{
    image_t originalImage, resultImage;
    FILE *infp, *outfp;
    parseArg(argc, argv, &infp, &outfp);

    readPgmRawHeader(infp, &originalImage);

    readPgmRawBitmapData(infp, &originalImage);

    initImage(&resultImage, originalImage.width, originalImage.height,
            originalImage.maxValue);

    filteringImage(&resultImage, &originalImage);

    writePgmRawHeader(outfp, &resultImage);

    writePgmRawBitmapData(outfp, &resultImage);

    return 0;
}
